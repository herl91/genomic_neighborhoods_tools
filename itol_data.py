from __future__ import division
import os
import pandas as pd

dom_width = 2000
triangle_width = dom_width / 3
rectangle_width = dom_width - triangle_width

igd_width_base = triangle_width * 1.5
full_width = dom_width + igd_width_base


def load_itol_style(style_folder):
    fname = os.path.join(style_folder, 'colors.csv')
    pfam_colors = {}
    with open(fname) as f:
        for i, line in enumerate(f):
            name, pfams, color = line.rstrip().split(',')
            for cur_pfam in pfams.split(';'):
                pfam_colors[cur_pfam] = {'name': name, 'color': color, 'priority': i}

    fname = os.path.join(style_folder, 'heatmap_tags.csv')
    heatmap_tags = {}
    with open(fname) as f:
        for line in f:
            name, tag = line.rstrip().split(',')
            heatmap_tags[name] = tag
    return pfam_colors, heatmap_tags


def load_itol_templates(style_folder):
    fname = os.path.join(style_folder, 'domains.template.txt')
    with open(fname) as f:
        dom_template = f.read()

    fname = os.path.join(style_folder, 'heatmap.template.txt')
    with open(fname) as f:
        heat_template = f.read()

    return dom_template, heat_template


def out_itol_data(out_folder, itol_cluster_data, itol_beads_data, itol_heatmap_data, dom_template, heat_template):
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    with open(os.path.join(out_folder, 'clusters.itol.txt'), 'a') as f:
        f.write(dom_template)
        for n in itol_cluster_data:
            newline = ','.join(n)
            f.write(newline)
            f.write('\n')

    with open(os.path.join(out_folder, 'beads.itol.txt'), 'a') as f:
        f.write(dom_template)
        for n in itol_beads_data:
            newline = ','.join(n)
            f.write(newline)
            f.write('\n')

    #cols = 'CYP702A,CYP705A,CYP706A,CYP708A,CYP716A,CYP735A,CYP71A,CYP72C,CYP76C,CYP81,ACT_IIIa,ACT_IIIb'.split(',')

    with open(os.path.join(out_folder, 'heatmap.itol.txt'), 'w') as f:
        f.write(heat_template)
        pd.DataFrame.from_dict(itol_heatmap_data, orient='index').replace(to_replace=0, value='X').to_csv(
                f, sep=',', header=False)


def get_domain_elements(strand, start, end, color, label, real_start, element_n):
    # cluster
    x = '|'.join(['RE',
                  str(start - real_start),
                  str(end - real_start),
                  color,
                  label])

    # bead with direction
    if strand == 1:  # right arrow
        y = (
                '|'.join(['RE',
                          str((element_n - 1) * full_width),
                          str(((element_n - 1) * full_width) + rectangle_width),
                          color,
                          label])
                + ',' +
                '|'.join(['TR',
                          str(((element_n - 1) * full_width) + rectangle_width),
                          str(((element_n - 1) * full_width) + rectangle_width + triangle_width),
                          color,
                          label])
        )
    else:  # left arrow
        y = (
                '|'.join(['TL',
                          str((element_n - 1) * full_width),
                          str(((element_n - 1) * full_width) + triangle_width),
                          color,
                          label])
                + ',' +
                '|'.join(['RE',
                          str(((element_n - 1) * full_width) + triangle_width),
                          str(((element_n - 1) * full_width) + triangle_width + rectangle_width),
                          color,
                          label])
        )

    return x, y


def itol_cluster(targets_df, cur_cluster, style_folder):
    from getNeighbors import intersect_this

    pfam_colors, heatmap_tags = load_itol_style(style_folder)

    clusters_fig = []
    beads_fig = []
    heatmap_dict = {}

    for cur_cluster_target in targets_df.gene:
        if cur_cluster_target not in heatmap_dict:
            heatmap_dict[cur_cluster_target] = {}
            for n in heatmap_tags.values():
                heatmap_dict[cur_cluster_target][n] = 0

        cur_cluster_size = cur_cluster.end.max() - cur_cluster.start.min()
        cur_cluster_real_start = cur_cluster.start.min()
        clusters_fig.append([cur_cluster_target, str(cur_cluster_size)])
        beads_fig.append([cur_cluster_target, 'X'])           # X = size of full cluster (dependant on elements number)

        element_n = 0   # initializing.
        for element_n, cur_element_index in enumerate(cur_cluster.index, start=1):
            c_element = ''
            b_element = ''
            cur_gene = cur_cluster.gene[cur_element_index]
            if cur_gene == cur_cluster_target:  # target gene (leaf)
                c_element, b_element = get_domain_elements(
                    cur_cluster.strand[cur_element_index],
                    cur_cluster.start[cur_element_index], cur_cluster.end[cur_element_index],
                    '#000000', 'target', cur_cluster_real_start, element_n)
            elif intersect_this(cur_cluster.pfam[cur_element_index], set(pfam_colors.keys())):  # recognized domain
                # TODO select one domain to draw...
                priority = len(pfam_colors)
                for n in cur_cluster.pfam[cur_element_index].split(','):
                    if n in pfam_colors and pfam_colors[n]['priority'] < priority:
                        c_element, b_element = get_domain_elements(
                            cur_cluster.strand[cur_element_index],
                            cur_cluster.start[cur_element_index], cur_cluster.end[cur_element_index],
                            pfam_colors[n]['color'], pfam_colors[n]['name'],
                            cur_cluster_real_start, element_n)
                        priority = pfam_colors[n]['priority']
            else:  # gray 	#808080                                                 # other domain
                if type(cur_cluster.pfam[cur_element_index]) == str:
                    thisdomain = cur_cluster.pfam[cur_element_index].replace(',', ';')
                else:
                    thisdomain = 'N/A'
                c_element, b_element = get_domain_elements(
                    cur_cluster.strand[cur_element_index],
                    cur_cluster.start[cur_element_index], cur_cluster.end[cur_element_index],
                    '#808080', thisdomain, cur_cluster_real_start, element_n)
            if c_element and b_element:
                clusters_fig[-1].append(c_element)
                beads_fig[-1].append(b_element)

            if cur_gene in heatmap_tags:
                cur_gene_tag = heatmap_tags[cur_gene]
                heatmap_dict[cur_cluster_target][cur_gene_tag] += 1

        # fix beads full size (dependant on number of elements)
        beads_fig[-1][1] = str((element_n * full_width) - igd_width_base)

    return clusters_fig, beads_fig, heatmap_dict
