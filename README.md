# Genomic Neighborhood Tools

## Installation
`git clone https://bitbucket.org/herl91/genomic_neighborhoods_tools`

`conda install -y --name genomic_neighborhoods --file requirements.txt`

`conda activate genomic_neighborhoods`

To use `plotClusterSimilarity`, the libraries `seqinr` and `gplots` must be installed in R.

# getNeighbors.py
This script identifies and isolates Genomic Neighborhoods (GNs) across GFF files by being given a specific target (see
below). The one-tailed Fisher's exact test is used to identify pfam doamins overrepresented across the GNs.

## Usage
`getNeighbors.py [options] gff_folder pfams_folder target output_folder`

## Input
#### Positional
* _gff_folder_: Folder containing one GFF3 file for each species to be analyzed.

* _pfams_folder_: Folder contatining one PFAM annotations file for each species.

* _target_: File or string according to target_type option (see below).

#### Optional
* _target_type_:

    * pfam: When using this option, the positional _target_ argument must be a string detailing which pfam domains to use to
build the GNs. A name must be specified apart from the domain name, and they must be separated by a colon, as seen
below:

`CYP:p450`
    
If more than one pfam domain must be used as target, each target mauy be separated by two colons, as seen below:
    
`CYP:p450::ACT:Transferase`
    
Finally, one can group multiple pfam domains (comma-separated) for the script to identify as one:
    
`TPS:Terpene_synth,Terpene_synth_C`

   * gene: When using this option, the positional _target_ argument must be a file. This file must be a two-column csv with
no header. Column 1 will be the genes around which GNs will be built, while the second column must be an annotation to
group GNs at the end (for example, a protein domain).

* _flank_size_: Determines the general size of GNs. By default, GNs are built by couting 5 genes on each flank of any gene
that matches the target, generating GNs that are 11 genes big. There are two exceptions: when GNs overlap, they are
merged, resulting in GNs that are larger than 11 genes, and when a target gene is in a scaffold smaller than 11 genes
big, resulting in GNs smaller than 11 genes big.

* _ignore_domains_: Provide a string of comma-separated pfam domains that will be excluded from the analysis.

* _itol_: [flag] If turned on, a folder will be generated providing annotations to visualize in ITOL.

# conservativeFisher.py
This script applies a conservative version of the one-tailed Fisher's exact test to find overrepresented domains in the
GNs by using a provided phylogeny. This script works by taking as input the output from `getNeighbors.py`.

## Usage
`conservativeFisher.py [options] summary_file traits_file newick_file output_summary`

## Input
#### Positional
* _summary_file_: This file is called `summary.csv` in the `output_folder` after running `getNeighbors.py`.

* _traits_file_: This file is called `traits.csv` in the `output_folder` after running `getNeighbors.py`.

* _newick_file_: A tree in Newick format, describing the phylogeny of the genes used as target in `getNeighbors.py`.

# plotClusterSimilarity.R
This script plots a heatmap to visualize sequence similarity between all members of two GNs or BGCs.

## Usage
`plotClusterSimilarity.R fasta_alignment vertical_cluster_ids horizontal_cluster_ids outfile`

## Input
#### Positional
* _fasta_alignment_: MSA in fasta format.

* _vertical_cluster_ids_: Ordered IDs of genes to as rows in the heatmap.

* _horizontal_cluster_ids_: Ordered IDs of genes to as columns in the heatmap.