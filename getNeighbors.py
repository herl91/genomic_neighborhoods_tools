#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import sys
import os
import argparse
import pandas as pd
import numpy as np
import scipy.stats as st
import itol_data as itdt


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('gff_folder',
                        type=str,
                        help='Folder containg a GFF3 for each genome.')
    parser.add_argument('pfams_folder',
                        type=str,
                        help='Folder containing annotation files. Annotation and GFF files for the same species must '
                             'have the same name (including file extension). Each annotation file must be a two-column '
                             'CSV with no header. Multiple PFAMS in one gene must be separated by a semicolon.')
    parser.add_argument('target',
                        type=str)
    parser.add_argument('output_folder',
                        type=str)
    parser.add_argument('--target_type', '-t',
                        type=str,
                        required=False,
                        choices=['pfam', 'genes'],
                        default='pfam',
                        help='''pfam: string => name:dom1,dom2,dom3::name:dom4[...]
                        genes: file''')
    parser.add_argument('--flank_size', '-n',
                        type=int,
                        required=False,
                        default=5)
    parser.add_argument('--itol',
                        required=False,
                        default=True,
                        action='store_true')
    parser.add_argument('--ignore_domains',
                        type=str,
                        required=False,
                        default='')
    return parser.parse_args()


def intersect_this(pfams_string, other_set):
    """
    :param str pfams_string:
    :param set other_set:
    :return:
    """
    if type(pfams_string) == float:
        return False
    else:
        s1 = set(pfams_string.split(';'))
        x = s1.intersection(other_set)
        if len(x) > 0:
            return True
        else:
            return False


def out_target_specific(out_folder, genome_targets, clusters_df):
    """
    :param pandas.core.frame.DataFrame clusters_df:
    :param str out_folder:
    :param dict genome_targets:
    :return:
    """
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    pd.DataFrame.from_dict(genome_targets, orient='index').to_csv(os.path.join(out_folder, 'targets_data.csv'),
                                                                  sep=',')

    clusters_df.to_csv(os.path.join(out_folder, 'clusters.csv'),
                       sep=',', index=False,
                       columns=['cluster', 'genome', 'record', 'gene', 'pfam', 'start', 'end'])


def get_gene_name(attributes):
    # ID, Name, Alias
    for cur_att in attributes.split(';'):
        if 'ID' in cur_att:
            return cur_att.split('=')[1]
        elif 'Name' in cur_att:
            return cur_att.split('=')[1]
        elif 'Alias' in cur_att:
            return cur_att.split('=')[1]
    return ''


def main():
    itol_style_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'itol_style')

    # INPUT
    if not os.path.exists(Options.output_folder):
        os.makedirs(Options.output_folder)
    with open(os.path.join(Options.output_folder, 'log'), 'w') as f:
        line = ' '.join(sys.argv) + '\n\n'
        f.write(line)
        for arg, value in sorted(vars(Options).items()):
            line = ' '.join([arg, str(value)]) + '\n'
            f.write(line)

    target_runs = []
    target_tags = {}
    if Options.target_type == 'pfam':
        for group in Options.target.split('::'):
            name_doms = group.split(':')
            name = name_doms[0]
            if len(name_doms) > 1:
                doms = set(name_doms[1].split(','))
            else:
                doms = set(name_doms)

            target_runs.append([name, list(doms)])     # why set anyway....
    else:  # genes
        target_tags = dict(zip(np.genfromtxt(Options.target, dtype='str', delimiter=',', usecols=[0]),
                               np.genfromtxt(Options.target, dtype='str', delimiter=',', usecols=[1])))
        for v in set(target_tags.values()):
            target_runs.append([v, [v]])

    # TODO unify this
    genomes_gff = set([n for n in os.listdir(Options.gff_folder)
                       if os.path.isfile(os.path.join(Options.gff_folder, n)) and
                       not n.startswith('.')])
    genomes_pfams = set([n for n in os.listdir(Options.pfams_folder)
                         if os.path.isfile(os.path.join(Options.pfams_folder, n)) and
                         not n.startswith('.')])
    genomes = genomes_gff.intersection(genomes_pfams)
    #

    Options.ignore_domains = Options.ignore_domains.split(',')

    dff = []
    cols = ['pfam', 'target', 'plant', 'n_targets', 'n_clusters', 'P', 'this_pfam_clustered', 'this_pfam_total',
            'all_clustered_pfams', 'all_total_pfams']

    # PIPELINE
    itol_cluster_data = []
    itol_beads_data = []
    itol_heatmap_data = {}
    cluster_bin_traits = {}
    cluster_nbin_traits = {}

    for target_name, cur_target_doms in target_runs:
        genome_targets = {}
        genome_total_pfams = {'all': {}}
        genome_clustered_pfams = {'all': {}}
        genome_clustered_pfams_individual = {'all': {}}
        clusters_df = pd.DataFrame(columns=['genome', 'cluster', 'gene', 'record', 'start', 'end', 'pfam'])

        for cur_genome in genomes:
            genome_clustered_pfams[cur_genome] = {}
            genome_clustered_pfams_individual[cur_genome] = {}
            genome_total_pfams[cur_genome] = {}

            cur_gff = Options.gff_folder + cur_genome
            cur_pp = Options.pfams_folder + cur_genome

            g_df = pd.read_csv(
                cur_gff, names=['record', 'source', 'type', 'start', 'end', 'score', 'strand', 'phase', 'attributes'],
                comment='#', index_col=None, delimiter='\t')
            p_df = pd.read_csv(cur_pp, names=['gene', 'pfam'], index_col=0, delimiter=',')

            # parse gff
            g_df = g_df[g_df['type'] == 'gene']
            genes = []
            for i, cur_row in g_df.iterrows():
                gene_name = get_gene_name(cur_row['attributes'])
                if not gene_name:
                    gene_name = '_'.join(['gene_at', cur_row['record'], cur_row['start'], cur_row['end']])
                genes.append(gene_name)
            g_df['gene'] = genes
            g_df = g_df.set_index('gene')

            # merge and sort
            cur_df = g_df.join(p_df)
            cur_df = cur_df.sort_values(by=['record', 'start']).reset_index(drop=False)

            # count pfams through whole genome
            for cur_gene_pfam in cur_df.pfam:
                if type(cur_gene_pfam) == float:  # nan
                    continue
                for cur_pfam in cur_gene_pfam.split(';'):
                    if cur_pfam not in genome_total_pfams[cur_genome]:
                        genome_total_pfams[cur_genome][cur_pfam] = 0
                    genome_total_pfams[cur_genome][cur_pfam] += 1
                    if cur_pfam not in genome_total_pfams['all']:
                        genome_total_pfams['all'][cur_pfam] = 0
                    genome_total_pfams['all'][cur_pfam] += 1

            # search targets
            if Options.target_type == 'pfam':
                target_mask = cur_df.pfam.apply(intersect_this, args=(cur_target_doms,))
            else:   # genes
                target_mask = cur_df.gene.isin(target_tags)

            targets = np.array(cur_df[target_mask].index.tolist())
            genome_targets[cur_genome] = {'targets': len(targets), 'clusters': 0}

            t_starts = targets - Options.flank_size
            t_ends = targets + Options.flank_size
            t_records = np.array(cur_df.loc[targets].record.tolist())

            used_targets = set()
            # search neighbors
            for i, cur_target in enumerate(targets):
                if cur_target in used_targets:
                    continue
                cur_record = cur_df.loc[cur_target].record

                # which clusters start before this one ends, and after this one started, in the same record
                to_merge = np.all([t_starts <= t_ends[i],
                                   t_starts >= t_starts[i],
                                   t_records == t_records[i]],
                                  axis=0)

                targets_df = cur_df.loc[targets[to_merge]]
                used_targets.update(targets_df.index)

                left_target = targets_df.index.min()
                right_target = targets_df.index.max()

                cur_cluster = cur_df[cur_df.record == cur_record].loc[
                              left_target - Options.flank_size:right_target + Options.flank_size
                              ]

                cur_cluster['cluster'] = len(clusters_df.cluster.unique()) + 1
                cur_cluster['genome'] = cur_genome

                # ITOL
                if Options.itol:
                    c, b, h = itdt.itol_cluster(targets_df, cur_cluster, itol_style_folder)
                    for n in c:
                        itol_cluster_data.append(n)
                    for n in b:
                        itol_beads_data.append(n)
                    itol_heatmap_data.update(h)

                clusters_df = pd.concat([clusters_df, cur_cluster], sort=True).reset_index(drop=True)
                cluster_pfams = cur_cluster.pfam.tolist()
                genome_targets[cur_genome]['clusters'] += 1

                # counting
                cur_cluster_used_pfams = set()
                for cur_gene_pfam in cluster_pfams:
                    if type(cur_gene_pfam) == float:
                        continue
                    else:
                        cur_gene_pfam = cur_gene_pfam.split(';')

                    for cur_pfam in cur_gene_pfam:
                        if cur_pfam not in genome_clustered_pfams[cur_genome]:
                            genome_clustered_pfams[cur_genome][cur_pfam] = 0
                        genome_clustered_pfams[cur_genome][cur_pfam] += 1
                        if cur_pfam not in genome_clustered_pfams['all']:
                            genome_clustered_pfams['all'][cur_pfam] = 0
                        genome_clustered_pfams['all'][cur_pfam] += 1
                        cur_cluster_used_pfams.add(cur_pfam)

                        for cur_iso_target in targets_df.gene:
                            if cur_iso_target not in cluster_bin_traits:
                                cluster_bin_traits[cur_iso_target] = {}  # dict with target name
                                cluster_nbin_traits[cur_iso_target] = {}  # dict with target name
                            if cur_pfam not in cluster_bin_traits[cur_iso_target]:
                                cluster_bin_traits[cur_iso_target][cur_pfam] = 1
                                cluster_nbin_traits[cur_iso_target][cur_pfam] = 1
                            else:
                                cluster_nbin_traits[cur_iso_target][cur_pfam] += 1

                # individual domain counting
                for cur_pfam in cur_cluster_used_pfams:
                    if cur_pfam not in genome_clustered_pfams_individual[cur_genome]:
                        genome_clustered_pfams_individual[cur_genome][cur_pfam] = 0
                    genome_clustered_pfams_individual[cur_genome][cur_pfam] += 1
                    if cur_pfam not in genome_clustered_pfams_individual['all']:
                        genome_clustered_pfams_individual['all'][cur_pfam] = 0
                    genome_clustered_pfams_individual['all'][cur_pfam] += 1

        # stats and summary
        genome_pvals = {}  # uncorrected
        list_pfams = []
        list_targets = []
        list_plants = []
        list_t = []
        list_c = []
        list_p = []  # bfr corrected
        list_k = []
        list_K = []
        list_n = []
        list_N = []
        genomes_and_all = list(genomes)
        genomes_and_all.append('all')
        for cur_genome in genomes_and_all:
            genome_pvals[cur_genome] = {}
            for cur_pfam in genome_clustered_pfams_individual[cur_genome]:
                if cur_pfam in Options.ignore_domains:
                    continue
                k = genome_clustered_pfams[cur_genome][cur_pfam]
                K = genome_total_pfams[cur_genome][cur_pfam]
                n = sum(genome_clustered_pfams[cur_genome].values())
                N = sum(genome_total_pfams[cur_genome].values())
                if k > 0:
                    genome_pvals[cur_genome][cur_pfam] = st.hypergeom.sf(k - 1, N, K, n)
                    #
                    list_k.append(k)
                    list_K.append(K)
                    list_n.append(n)
                    list_N.append(N)
                    if cur_genome == 'all':
                        list_t.append(
                            sum([genome_dict['targets'] for genome_dict in genome_targets.values()]))
                        list_c.append(
                            sum([genome_dict['clusters'] for genome_dict in genome_targets.values()]))
                    else:
                        list_t.append(genome_targets[cur_genome]['targets'])
                        list_c.append(genome_targets[cur_genome]['clusters'])
                    list_pfams.append(cur_pfam)
                    list_targets.append(target_name)
                    list_plants.append(cur_genome)

        # bonferroni corr
        for cur_genome in genome_pvals:
            m = len(genome_pvals[cur_genome])
            for cur_pfam in genome_pvals[cur_genome]:
                genome_pvals[cur_genome][cur_pfam] *= m

        for i, this_pfam in enumerate(list_pfams):
            # this_genome = '_'.join(list_targets[i].split('_')[-2:])
            this_genome = list_plants[i]
            list_p.append(genome_pvals[this_genome][this_pfam])

        # update
        dff.append(pd.DataFrame(data={'pfam': list_pfams,
                                      'target': list_targets,
                                      'plant': list_plants,
                                      'n_targets': list_t,
                                      'n_clusters': list_c,
                                      'P': list_p,
                                      'this_pfam_clustered': list_k,
                                      'this_pfam_total': list_K,
                                      'all_clustered_pfams': list_n,
                                      'all_total_pfams': list_N}))

        # out
        out_target_specific(os.path.join(Options.output_folder, target_name), genome_targets, clusters_df)

    # out general
    dff = pd.concat(dff)
    dff[cols].to_csv(os.path.join(Options.output_folder, 'summary.csv'), index=False)

    # out bin traits
    cluster_bin_traits_df = pd.DataFrame.from_dict(cluster_bin_traits, orient='index').fillna(0)
    cluster_bin_traits_df.to_csv(os.path.join(Options.output_folder, 'traits_bin.csv'), index=True)

    # out nbin traits
    cluster_nbin_traits_df = pd.DataFrame.from_dict(cluster_nbin_traits, orient='index').fillna(0)
    cluster_nbin_traits_df.to_csv(os.path.join(Options.output_folder, 'traits.csv'), index=True)

    # itol optional
    if Options.itol:
        dom_template, heat_template = itdt.load_itol_templates(itol_style_folder)
        itdt.out_itol_data(os.path.join(Options.output_folder, 'itol/'), itol_cluster_data, itol_beads_data,
                           itol_heatmap_data,
                           dom_template, heat_template)


if __name__ == "__main__":
    Options = get_args()
    main()
