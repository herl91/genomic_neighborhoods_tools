#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import argparse
import pandas as pd
import scipy.stats as st
import ete3 as et


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('summary_file',
                        type=str)
    parser.add_argument('traits_file',
                        type=str)
    parser.add_argument('newick_file',
                        type=str)
    parser.add_argument('output_summary',
                        type=str)
    return parser.parse_args()


def main():
    # INPUT
    summary_orig = pd.read_csv(Options.summary_file)
    plant_mask = summary_orig.plant == 'all'
    summary_orig = summary_orig[plant_mask]

    traits = pd.read_csv(Options.traits_file, index_col=0)
    tree = et.Tree(Options.newick_file)
    tree_leaves = set([leaf.name for leaf in tree.iter_leaves()])

    # Add features
    for cur_leaf in traits.index:
        if cur_leaf in tree:
            for cur_trait in traits:
                value = traits.loc[cur_leaf][cur_trait]
                (tree & cur_leaf).add_feature(cur_trait, bool(value))
                (tree & cur_leaf).add_feature(cur_trait + '_real_count', value)

    # retest
    # get data (selected bins)
    list_new_k = []
    list_new_K = []
    all_new_p_c = []
    all_new_p_cf = []
    for cur_target in summary_orig.target.unique():
        target_mask = traits[cur_target] > 0
        target_leaves = set(traits[target_mask].index)
        target_leaves = target_leaves.intersection(tree_leaves)

        target_tree = tree.copy('deepcopy')
        target_tree.prune(target_leaves)

        target_mask = summary_orig.target == cur_target

        list_new_p_c = []
        list_new_p_cf = []
        for i, cur_row in summary_orig[target_mask].iterrows():
            cur_pfam = cur_row['pfam']

            # data for fisher
            # k = green marbles selected
            # K = green marbles total
            # n = all marbles selected
            # N = all marbles totals
            new_k = 0
            k = cur_row['this_pfam_clustered']
            K = cur_row['this_pfam_total']
            n = cur_row['all_clustered_pfams']
            N = cur_row['all_total_pfams']

            # data from tree
            # monophyly takes the largest quantity in each monophyletic group.
            # say between 3 leaves they have 4, 3 and 2 targets: take 4.
            mono_groups_gen = target_tree.get_monophyletic([True], cur_pfam)
            for cur_mono in mono_groups_gen:
                # max_mono_value = max([cur_leaf.__getattribute__(cur_pfam) for cur_leaf in cur_mono.iter_leaves()])
                max_mono_value = max([traits[cur_pfam][cur_leaf.name] for cur_leaf in cur_mono.iter_leaves()])
                new_k += max_mono_value

            # Should we treat K?
            # We cannot know the real value of new_K because we don't know if the unselected marbles are phylo grouped
            # But we can approximate it by substracting the difference between k and new_k
            new_K = K - (k - new_k)

            # retest
            conservative_P = st.hypergeom.sf(new_k - 1, N, K, n)
            conservative_fair_P = st.hypergeom.sf(new_k - 1, N, new_K, n)

            list_new_k.append(new_k)
            list_new_K.append(new_K)
            list_new_p_c.append(conservative_P)
            list_new_p_cf.append(conservative_fair_P)

        # bonferroni correction
        list_new_p_c = [n * len(list_new_p_c) for n in list_new_p_c]
        list_new_p_cf = [n * len(list_new_p_cf) for n in list_new_p_cf]

        # add to all_lists
        for n in list_new_p_c:
            all_new_p_c.append(n)
        for n in list_new_p_cf:
            all_new_p_cf.append(n)

    # update data
    summary_orig['P_conservative'] = all_new_p_c
    summary_orig['P_conservative_fair'] = all_new_p_cf
    summary_orig['this_pfam_clustered_conservative'] = list_new_k
    summary_orig['this_pfam_total_conservative'] = list_new_K

    summary_orig = summary_orig[['pfam',
                                 'target',
                                 'plant',
                                 'n_targets',
                                 'n_clusters',
                                 'P',
                                 'P_conservative',
                                 'P_conservative_fair',
                                 'this_pfam_clustered',
                                 'this_pfam_total',
                                 'this_pfam_clustered_conservative',
                                 'this_pfam_total_conservative',
                                 'all_clustered_pfams',
                                 'all_total_pfams']]

    # out
    summary_orig.to_csv(Options.output_summary, index=False)


if __name__ == "__main__":
    Options = get_args()
    main()
